# Luhn-app

Simple [SvelteKit](https://kit.svelte.dev/) app to test credit card number using
 [Luhn algorithm](https://en.wikipedia.org/wiki/Luhn_algorithm)

## Installing

```bash
npm install
```

## Running

```bash
npm run dev
```