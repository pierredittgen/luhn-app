function doubleVal(value: number): number {
	const dValue = value * 2;
	return dValue > 9 ? dValue - 9 : dValue;
}
export class Luhn {
	static valid(text: string): boolean {
		// Characters other than space and digits are invalid
		if (text.match(/[^ 0-9]/)) {
			return false;
		}
		// Turn text into digit list
		let digitList = [...text.replace(/ /g, '').split('').map(Number)];

		// Too short to be valid
		if (digitList.length <= 1) {
			return false;
		}

		// Compute sum of 'normal' and 'doubled' value
		let sum = digitList
			.reverse()
			.map((value, idx) => (idx % 2 == 0 ? value : doubleVal(value)))
			.reduce((acc, elt) => acc + elt, 0);

		// Valid if evenly divisible by 10
		return sum % 10 == 0;
	}
}
